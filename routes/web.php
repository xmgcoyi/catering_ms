<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('events.create');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/new-event', 'EventController@create')->name('events.create');

Route::post('events', 'EventController@store');

Route::middleware('event.exists')
    ->get('/add-meals/{event_id}', 'MealController@add')
    ->where('event_id', '[0-9]+')
    ->name('meals.add');

Route::middleware('event.exists')
    ->post('/meals', 'MealController@store');
