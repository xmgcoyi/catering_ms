<?php

namespace App\Http\Middleware;

use App\Event;
use Closure;

class EventExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $event = Event::find($request->event_id);

        if ($event === null) {
            return abort(404, 'Event not found!');
        }

        return $next($request);
    }
}
