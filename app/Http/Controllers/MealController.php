<?php

namespace App\Http\Controllers;

use App\Event;
use App\Meal;
use App\MealType;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MealController extends Controller
{
    public function add($eventId)
    {
        $event = Event::find($eventId);

        dump($event->meal_types);

        exit;

        return view('meals/add', [
            'event' => Event::find($eventId),
            'mealTypes' => MealType::pluck('name', 'id')
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, Meal::$rules);

        $meal = new Meal(Input::all());
        $meal->save();

        return redirect()->route('meals.add', ['event_id' => $meal->event->id]);
    }
}
