<?php

namespace App\Http\Controllers;

use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Google_Client;
use Google_Service_Books;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function create()
    {
//        $client = $this->getClient();
//        $service = new \Google_Service_Calendar($client);

// Print the next 10 events on the user's calendar.
//        $calendarId = 'ga5678kops8ru067ebqv1ifivk@group.calendar.google.com';

//        $start = (new Carbon())->toRfc3339String();
//        $end = (new Carbon('2pm'))->toRfc3339String();

//        echo $start->toRfc3339String() . ' - ' . $end; exit;

        //RFC3339
//       $event = new \Google_Service_Calendar_Event(array(
//           'summary' => 'Testing test test',
//           'location' => '800 Howard St., San Francisco, CA 94103',
//           'description' => 'A chance to hear more about Google\'s developer products.',
//           'start' => array(
//               'dateTime' => $start,
//               'timeZone' => 'Africa/Johannesburg',
//           ),
//           'end' => array(
//               'dateTime' => $end,
//               'timeZone' => 'Africa/Johannesburg',
//           ),
//           'recurrence' => array(
//               'RRULE:FREQ=DAILY;COUNT=2'
//           ),
//           'attendees' => array(
//               array('email' => 'lpage@example.com'),
//               array('email' => 'sbrin@example.com'),
//           ),
//           'reminders' => array(
//               'useDefault' => FALSE,
//               'overrides' => array(
//                   array('method' => 'email', 'minutes' => 24 * 60),
//                   array('method' => 'popup', 'minutes' => 10),
//               ),
//           ),
//       ));
//
//       $event = $service->events->insert($calendarId, $event);


        // $service->events->quickAdd($calendarId, 'hello world');

        return view('events/create');
    }

    function getClient() {
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->expandHomeDirectory(CREDENTIALS_PATH);
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if(!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    /**
     * Expands the home directory alias '~' to the full path.
     * @param string $path the path to expand.
     * @return string the expanded path.
     */
    function expandHomeDirectory($path) {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }


    public function store(Request $request)
    {

        $this->validate($request, Event::$rules);

        $event = new Event($request->all());

        $event->save();

        return redirect()->route('meals.add', ['event_id' => $event->id])->with('success', 'Event saved!');
    }
}
