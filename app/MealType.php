<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealType extends Model
{
    public function meals()
    {
        return $this->hasMany('App\Meal');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event', 'meals')->using('App\Meal');
    }
}
