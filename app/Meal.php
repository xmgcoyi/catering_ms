<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{

    protected $fillable = [
        'name', 'quantity', 'details', 'special_instructions', 'meal_type_id', 'event_id'
    ];

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function meal_type()
    {
        return $this->belongsTo('App\MealType');
    }

    public static $rules = [
        'name' => 'required',
        'meal_type_id' => 'required|integer',
        'event_id' => 'required|integer',
        'quantity' => 'integer'
    ];
}
