<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Validator;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location', 'address', 'headcount', 'details',  'event_date', 'contact_name', 'contact_email',
        'contact_number',
    ];

    public static $rules = [
        'name' => 'required',
        'headcount' => 'integer',
        'event_date' => 'date|after:now',
        'contact_email' => 'email',
        'contact_number' => 'digits_between:8,10'
    ];

    public function meals()
    {
        return $this->hasMany('App\Meal');
    }

    public function meal_types()
    {
        return $this->belongsToMany('App\MealType', 'meals')->using('App\Meal');
    }

    public function hasMeals()
    {
        return $this->meals()->exists();
    }
}
