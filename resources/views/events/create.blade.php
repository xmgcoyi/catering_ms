@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="panel panel-default">
                    <div class="panel-heading h1">Create New Event</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="/events">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="name" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                <label for="location" class="col-md-4 control-label">Location</label>

                                <div class="col-md-6">
                                    <input id="location" class="form-control" name="location" value="{{ old('location') }}" required autofocus>

                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <input id="address" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('headcount') ? ' has-error' : '' }}">
                                <label for="headcount" class="col-md-4 control-label">Head Count</label>

                                <div class="col-md-6">
                                    <input id="headcount" class="form-control" name="headcount" value="{{ old('headcount') }}" required autofocus>

                                    @if ($errors->has('headcount'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('headcount') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('event_date') ? ' has-error' : '' }}">
                                <label for="event_date" class="col-md-4 control-label">Event Date</label>

                                <div class="col-md-6">
                                    <input id="event_date" class="form-control" name="event_date" value="{{ old('event_date') }}" required autofocus>

                                    @if ($errors->has('event_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('event_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('contact_name') ? ' has-error' : '' }}">
                                <label for="contact_name" class="col-md-4 control-label">Contact Name</label>

                                <div class="col-md-6">
                                    <input id="contact_name" class="form-control" name="contact_name" value="{{ old('contact_name') }}" required autofocus>

                                    @if ($errors->has('contact_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contact_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                                <label for="contact_number" class="col-md-4 control-label">Contact Number</label>

                                <div class="col-md-6">
                                    <input id="contact_number" class="form-control" name="contact_number" value="{{ old('contact_number') }}" required autofocus>

                                    @if ($errors->has('contact_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contact_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('contact_email') ? ' has-error' : '' }}">
                                <label for="contact_email" class="col-md-4 control-label">Contact Email</label>

                                <div class="col-md-6">
                                    <input id="contact_email" class="form-control" name="contact_email" value="{{ old('contact_email') }}" required autofocus>

                                    @if ($errors->has('contact_email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contact_email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
                                <label for="details" class="col-md-4 control-label">Details</label>

                                <div class="col-md-6">
                                    <input id="details" class="form-control" name="details" value="{{ old('details') }}" required autofocus>

                                    @if ($errors->has('details'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php /*
                    <div class="panel-body">
                        <iframe src="https://calendar.google.com/calendar/embed?src=ga5678kops8ru067ebqv1ifivk%40group.calendar.google.com&ctz=Africa%2FJohannesburg" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                    </div>
 */ ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('before_body_close')
<script>
    $("#event_date").flatpickr({
        enableTime: true,
        minDate: "today"
    });
</script>
@endsection
