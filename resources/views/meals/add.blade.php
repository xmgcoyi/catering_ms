@extends('layouts.app')


@section('content')
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-default">
                <div class="panel-heading h1">{{  $event->name }}</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="col-md-4 col-sm-4 text-right">
                                <strong>Location: </strong>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {{ $event->location }}
                            </div>

                            <div class="col-md-4 col-sm-4 text-right">
                                <strong>Address: </strong>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {{ $event->address }}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="col-md-4 col-sm-4 text-right">
                                <strong>Headcount: </strong>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {{ $event->headcount }}
                            </div>

                            <div class="col-md-4 col-sm-4 text-right">
                                <strong>Details: </strong>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {{ $event->details }}
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Table -->
                @if( $event->hasMeals() )
                <table class="table">
                    <thead>
                        <th>Items</th>
                        <th>Details</th>
                    </thead>
                    <tbody>
                        @foreach($event->meals as $meal)
                        <tr>
                            <td>{{ $meal->name }}</td>
                            <td>{{ $meal->details }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-default">
                <div class="panel-heading h2">Add Meal</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="/meals">
                        {{ csrf_field() }}

                        <input name="event_id" value="{{ $event->id }}" type="hidden" />

                        <div class="form-group{{ $errors->has('meal_type_id') ? ' has-error' : '' }}">
                            <label for="meal_type" class="col-md-4 control-label">Meal Type</label>

                            <div class="col-md-6">
                                {{--<input id="meal_type" class="form-control" name="meal_type" value="{{ old('meal_type') }}" required autofocus>--}}

                                <select id="meal_type" class="form-control" name="meal_type_id" value="{{ old('meal_type') }}" required autofocus>
                                    @foreach($mealTypes as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('meal_type_id'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('meal_type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('instructions') ? ' has-error' : '' }}">
                            <label for="instructions" class="col-md-4 control-label">Special Instructions</label>

                            <div class="col-md-6">
                                <input id="instructions" class="form-control" name="instructions" value="{{ old('instructions') }}" required autofocus>

                                @if ($errors->has('instructions'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('instructions') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                            <label for="quantity" class="col-md-4 control-label">Quantity</label>

                            <div class="col-md-6">
                                <input id="quantity" class="form-control" name="quantity" value="{{ old('quantity') }}" required autofocus>

                                @if ($errors->has('quantity'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
                            <label for="details" class="col-md-4 control-label">Details</label>

                            <div class="col-md-6">
                                <input id="details" class="form-control" name="details" value="{{ old('details') }}" required autofocus>

                                @if ($errors->has('details'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php /*
                    <div class="panel-body">
                        <iframe src="https://calendar.google.com/calendar/embed?src=ga5678kops8ru067ebqv1ifivk%40group.calendar.google.com&ctz=Africa%2FJohannesburg" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                    </div>
 */ ?>
            </div>
        </div>
    </div>

    @foreach($mealTypes as $id => $name)
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-default">
                <div class="panel-heading h1">{{  $event->name }}</div>
                <!-- Table -->
                @if( $event->hasMeals() )
                    <table class="table">
                        <thead>
                        <th>Items</th>
                        <th>Details</th>
                        </thead>
                        <tbody>
                        @foreach($event->meals as $meal)
                            <tr>
                                <td>{{ $meal->name }}</td>
                                <td>{{ $meal->details }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
